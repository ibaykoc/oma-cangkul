﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : Hand {

	WaitForSeconds waitHalfSec = new WaitForSeconds(0.5f);

	void OnEnable(){

		GameManager.instance.OnStartTurn += PasangKartu;

	}

	void OnDisable(){

		GameManager.instance.OnStartTurn -= PasangKartu;

	}

	void PasangKartu(){
		if (GameManager.instance.isPlayerTurn)
			return;
		StartCoroutine (IPasangKartu ());

	}

	IEnumerator IPasangKartu(){
		//print("Saatnya enemy pasang kartu");
		if (GameManager.instance.inGameCard.cards.Count <= 0) {				//Kartu pada game kosong (Bebas memasang kartu apapun)
			//print("Kartu pada game kosong");
			Card cardToPut = cards [Random.Range (0, cards.Count)];
			yield return StartCoroutine (AudioManager.instance.PlayRhomaPasang ());
			cardToPut.Show = true;
			GameManager.instance.PasangKartu (cardToPut);					//Pasang kartu random yang ada di tangan enemy

		} else {															//Terdapat kartu pada game
			//print("Kartu pada game ada " + GameManager.instance.inGameCard.cards.Count.ToString());
			Card cardInGame = GameManager.instance.inGameCard.cards [0];
			List<Card> cardCanPut = new List<Card> ();
			for (int i = 0; i < cards.Count; i++) {
				if (cardInGame.typeId == cards [i].typeId || cardInGame.typeId == -1)
					cardCanPut.Add (cards [i]);
			}
			if (cardCanPut.Count > 0) {										//Enemy mempunyai kartu yang cocok untuk dipasang
				//print("Enemy mempunyai " + cardCanPut.Count.ToString() + " kartu yang cocok");
				int cardID = Random.Range (0, cardCanPut.Count);
				yield return StartCoroutine (AudioManager.instance.PlayRhomaPasang ());
				cardCanPut [cardID].Show = true;
				GameManager.instance.PasangKartu (cardCanPut [cardID]);		//Memasang kartu acak yang cocok yang terdapat pada tangan enemy
			} else {														//Enemy tidak mempunyai kartu yang cocok untuk dipasang
				//print ("Saatnya cangkul kartu");
				yield return StartCoroutine (AudioManager.instance.PlayRhomaKecewa ());
				Card newCard = null;
				bool getOne = false;
				while (!getOne) {											//Mencangkul kartu selama belum menemukan kartu yang cocok untuk dipasang
					newCard = GameManager.instance.AmbilKartu (1);
					AudioManager.instance.PlayDealingCard ();
					if (newCard == null) {
						//print ("Deck habis");
						yield break;
					}
					Add (newCard, false);
					yield return waitHalfSec;
					if (newCard.typeId == cardInGame.typeId) {
						getOne = true;
						newCard.Show = true;
						GameManager.instance.PasangKartu (newCard);			//Memasang kartu acak yang cocok
						//print("Dapat kartu yang cocok, pasang!");
					}
				}
			}
		}
	}
}
