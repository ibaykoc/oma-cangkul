﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance;
	public delegate void StartTurn();
	public event StartTurn OnStartTurn;
	public delegate void Review();
	public event Review OnReview;
	public delegate void Done();
	public event Done OnDoneTurn;
	public delegate void myDelegate();
	public delegate void GameEnd();
	public event GameEnd OnGameEnd;

	public Hand[] hands;

	public InGameCard inGameCard;

	public Bin bin;

	public Card[] cards = new Card[52];

	public int currentDeckId = 0;

	public bool isPlayerTurn;

	public bool pause;

	//Stats
	bool gamePlaying = false;
	float matchTime;

	void Awake(){
		if (instance == null)
			instance = this;
		else
			Destroy (gameObject);
	}

	void Start(){
		if (SceneManager.GetActiveScene () == SceneManager.GetSceneByName ("Main Game")) {
			gamePlaying = true;
			matchTime = 0;
			InitializeCards ();
			KocokKartu ();
			StartCoroutine (IBagikanKartu ());
		}
	}

	void Update(){

		if (!gamePlaying)
			return;
		matchTime += Time.deltaTime;


	}

	void InitializeCards(){

		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0:
				for (int j = 0; j < 13; j++) {
					cards [(13 * i) + j] = DeckPrefab.instance.hearts [j];
				}
				break;
			case 1:
				for (int j = 0; j < 13; j++) {
					cards [(13 * i) + j] = DeckPrefab.instance.clubs [j];
				}
				break;
			case 2:
				for (int j = 0; j < 13; j++) {
					cards [(13 * i) + j] = DeckPrefab.instance.diamonds [j];
				}
				break;
			case 3:
				for (int j = 0; j < 13; j++) {
					cards [(13 * i) + j] = DeckPrefab.instance.spades [j];
				}
				break;
			case 4:
				for (int j = 0; j < 2; j++) {
					cards [(13 * i) + j] = DeckPrefab.instance.joker;
				}
				break;
			}

		}

	}

	void KocokKartu(){
		Card temp;
		int r;
		for (int i = 0; i < cards.Length; i++) {
			temp = cards [i];
			r = Random.Range (i, cards.Length);
			cards [i] = cards [r];
			cards [r] = temp;
		}

	}

	IEnumerator IBagikanKartu(){
		Card[] cards = new Card[7];
		for (int i = 0; i < hands.Length; i++) {
			for (int j = 0; j < 7; j++) {									//Total Kartu Per Tangan
				cards [j] = AmbilKartu (i);
			}

			yield return StartCoroutine (hands [i].IAdd (cards,(i==0)));
		}
		yield return StartCoroutine(AudioManager.instance.PlayRhomaMulai ());
		AudioManager.instance.PlayDealingCard ();
		yield return StartCoroutine(inGameCard.IAdd (AmbilKartu (-1)));

	}

	public Card AmbilKartu(int _toHandId){
		//print((_toHandId == 0)?"Player mengambil kartu":"Enemy mengambil kartu");
		if (currentDeckId == -100) {									//Jika deck habis
			if (!isHaveValidCard ()) {

				Card cardToTake = inGameCard.cards [0];
				if (isPlayerTurn) {										//Jika giliran player

					hands [0].Add (cardToTake, true);					//Player mengambil kartu dari inGameCard
					cardToTake.handId = 0;
					isPlayerTurn = false;

				} else {												//Jika giliran enemy

					hands [1].Add (cardToTake, false);					//Enemy mengambil kartu dari ingameCard
					StartCoroutine(AudioManager.instance.PlayRhomaKecewa());
					cardToTake.handId = 1;
					isPlayerTurn = true;
				}

				inGameCard.cards.Remove (cardToTake);
				cardToTake.state = Card.CardState.inHand;
				TurnEnd ();
			}

			return null;
		}
		GameObject go = Instantiate (cards [currentDeckId].gameObject, DeckPrefab.instance.transform.position, Quaternion.identity) as GameObject;
		currentDeckId++;
		if (currentDeckId >= cards.Length) {
			Destroy (GameObject.FindGameObjectWithTag ("Deck"));
			currentDeckId = -100;
		}
		Card card = go.GetComponent<Card> ();
		card.handId = _toHandId;
		if (currentDeckId == -100) {									//Jika deck habis
			if (!isHaveValidCard ()) {

				Card cardToTake = inGameCard.cards [0];
				if (isPlayerTurn) {										//Jika giliran player

					hands [0].Add (cardToTake, true);
					cardToTake.handId = 0;
					isPlayerTurn = false;

				} else {												//Jika giliran enemy

					hands [1].Add (cardToTake, false);
					cardToTake.handId = 1;
					isPlayerTurn = true;
				}

				inGameCard.cards.Remove (cardToTake);
				cardToTake.state = Card.CardState.inHand;
				TurnEnd ();
			}
		}

		return card;
	}

	public bool PasangKartu(Card _card){

		if (_card.handId == 1) {							//Jika yang memasang kartu adalah enemy

			hands [_card.handId].hasPutCard = true;
			isPlayerTurn = true;
			StartCoroutine(inGameCard.IAdd (_card));
			hands [_card.handId].Remove (_card);
			AudioManager.instance.PlayDealingCard ();
			return true;

		}else{												//Jika yang memasang kartu adalah player
			if (inGameCard.cards.Count <= 0) {				//Jika kartu dalam game kosong
				hands [_card.handId].hasPutCard = true;
				isPlayerTurn = false;
				StartCoroutine(inGameCard.IAdd (_card));
				hands [_card.handId].Remove (_card);
				AudioManager.instance.PlayDealingCard ();
				return true;
			} else {										//Jika terdapat kartu didalam game

				Card card = inGameCard.cards [0];

				if (card.typeId == _card.typeId) {			//Jika kartu yang dipasang player cocok
					hands [_card.handId].hasPutCard = true;
					isPlayerTurn = false;
					StartCoroutine(inGameCard.IAdd (_card));
					hands [_card.handId].Remove (_card);
					AudioManager.instance.PlayDealingCard ();
					return true;
				} else {									//Jika kartu yang dipasang player tidak cocok

					return false;

				}
			}
		}		
	}

	public void BuangKartu(Card _card){

		bin.Add (_card);

	}

	public bool IsSemuaPasangKartu(){
		foreach (var hand in hands) {
			if (!hand.hasPutCard) {
				return false;
			}
		}
		return true;
	}

	public void ReviewStart(){
		foreach (var hand in hands) {
			if (hand.cards.Count <= 0) {
				gamePlaying = false;
				RecordStats (GameManager.instance.isPlayerTurn);
				if (OnGameEnd != null)
					OnGameEnd ();
				return;
			}
		}
		if (IsSemuaPasangKartu ()) {							//Jika semua sudah memasang kartu	
			foreach (var hand in hands) {
				hand.hasPutCard = false;						//Semua tangan di set belum memasang kartu
			}
			if (OnReview != null)
				OnReview ();									//Review kartu
		} else {												//Jika hanya satu yang memasang kartu

			if (currentDeckId == -100) {							//Jika deck sudah habis
				if (!isHaveValidCard ()) {								//Jika tidak punya kartu yang valid
					Card cardToTake = inGameCard.cards [0];
					if (isPlayerTurn) {										//Jika giliran player

						hands [0].Add (cardToTake, true);
						cardToTake.handId = 0;
						isPlayerTurn = false;

					} else {												//Jika giliran enemy

						hands [1].Add (cardToTake, false);
						cardToTake.handId = 1;
						isPlayerTurn = true;
					}
					cardToTake.state = Card.CardState.inHand;
					inGameCard.cards.Remove (cardToTake);
				}
			}
		}
		TurnEnd ();											//Giliran selesai
	}

	public void TurnEnd(){

		foreach (var hand in hands) {
			if (hand.cards.Count <= 0) {					//Salah satu pemain habis kartu
				gamePlaying = false;
				RecordStats (GameManager.instance.isPlayerTurn);
				if (OnGameEnd != null)
					OnGameEnd ();
				return;
			}
		}

		if (OnDoneTurn != null) {
			OnDoneTurn ();
			//print ("Giliran selesai");
		}
	}

	public void TurnStart(){

		if(OnStartTurn != null)
			OnStartTurn ();

	}

	public bool isHaveValidCard(){

		if (inGameCard.cards.Count <= 0)
			return true;

		if (isPlayerTurn) {											//Jika giliran player

			foreach (Card card in hands[0].cards) {
				if (card.typeId == inGameCard.cards [0].typeId)		//Jika ada tipe kartu yang sama dengan kartu dalam game
					return true;
			}
			return false;
		} else {													//Jika giliran enemy

			foreach (Card card in hands[1].cards) {
				if (card.typeId == inGameCard.cards [0].typeId)		//Jika ada tipe kartu yang sama dengan kartu dalam game
					return true;
			}
			return false;

		}
	}

	public void Restart(){

		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

	}

	public void Pause(){

		pause = !pause;
		GameUI.instance.ShowPausePanel (pause);
	}

	public void RecordStats(bool _cpuWin){

//		print ("Stats Recorded");
		PlayerPrefs.SetInt ((!_cpuWin)?"Win":"Loss",PlayerPrefs.GetInt ((!_cpuWin)?"Win":"Loss") + 1);
		PlayerPrefs.SetFloat ("Total Play Time",PlayerPrefs.GetFloat("Total Play Time") + matchTime);
		if (PlayerPrefs.GetFloat ("Longest Match Time") < matchTime)
			PlayerPrefs.SetFloat ("Longest Match Time", matchTime);
		if (PlayerPrefs.GetFloat ("Shortest Match Time") > matchTime || PlayerPrefs.GetFloat ("Shortest Match Time") == 0)
			PlayerPrefs.SetFloat ("Shortest Match Time", matchTime);
		
	}
}
