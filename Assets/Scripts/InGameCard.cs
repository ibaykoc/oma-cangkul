﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InGameCard : MonoBehaviour {

	public Vector3 minPos, maxPos;
	public List<Card> cards = new List<Card>();

	void OnEnable(){

		GameManager.instance.OnReview += Review;

	}

	void OnDisable(){

		GameManager.instance.OnReview -= Review;

	}

//	public void Add(Card _card){
//		_card.state = Card.CardState.inGame;
//		cards.Add (_card);
//		UpdatePosition (true);
//
//	}

	public IEnumerator IAdd(Card _card){
		_card.state = Card.CardState.inGame;
		cards.Add (_card);
		yield return StartCoroutine (IUpdatePosition(true));

	}

	Vector3 FindPosition(int _id, int _total){

		return Vector3.Lerp(maxPos,minPos,(float)_id/(float)_total);

	}

	public void Remove(Card _card){

		cards.Remove (_card);
		UpdatePosition (false);
	}

	public void UpdatePosition(bool _reviewAtTheEnd){

		for (int i = cards.Count - 1; i >= 0; i--) {
			if(i==0 && _reviewAtTheEnd)
				StartCoroutine (cards[i].IMove(FindPosition(i + 1,cards.Count + 1),GameManager.instance.ReviewStart));
			else
				StartCoroutine (cards[i].IMove(FindPosition(i + 1,cards.Count + 1)));
		}

	}

	public IEnumerator IUpdatePosition(bool _reviewAtTheEnd){

		for (int i = cards.Count - 1; i >= 0; i--) {
			if(i==0 && _reviewAtTheEnd)
				yield return StartCoroutine (cards[i].IMove(FindPosition(i + 1,cards.Count + 1),GameManager.instance.ReviewStart));
			else
				yield return StartCoroutine (cards[i].IMove(FindPosition(i + 1,cards.Count + 1)));
		}

	}

	public void Review(){
		//print ("Saatnya review");
		int score = -10;
		Card winCard = null;
		for (int i = 0; i < cards.Count; i++) {
			if (cards [i].handId == -1)
				continue;
			if (cards [i].score > score) {

				score = cards [i].score;
				winCard = cards [i];

			}

		}
		if (winCard.handId == 0) {											//Player menang
		
			if (cards [cards.Count - 1].handId == 0) {						//Jika yang memasang kartu terakhir adalah player
				StartCoroutine (AudioManager.instance.PlayRhomaKecewa ());
			}
			GameManager.instance.isPlayerTurn = true;

		} else {															//Enemy menang

			GameManager.instance.isPlayerTurn = false;

		}
		AudioManager.instance.PlayDealingCard ();
		for (int i = cards.Count - 1; i >= 0; i--) {

			GameManager.instance.BuangKartu(cards[i]);
			Remove (cards [i]);

		}
	}
}
