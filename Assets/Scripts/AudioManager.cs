﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour {

	public static AudioManager instance;
	public AudioSource SFXAudioSource;
	public AudioSource BGAudioSource;
	[SerializeField] AudioSource rhomaAudioSource;

	public AudioClip dealingCard;
	[SerializeField] AudioClip[] rhomaMulai;
	[SerializeField] AudioClip[] rhomaKecewa;
	[SerializeField] AudioClip[] rhomaPasang;
	[SerializeField] AudioClip[] rhomaSelesai;
	public bool sfxOn = true;
	void Awake(){

		if (instance == null)
			instance = this;
		else
			Destroy (gameObject);

		SFXAudioSource = GetComponent<AudioSource> ();
		DontDestroyOnLoad (this);
	}

	public void PlayDealingCard(){

		if(sfxOn)
			SFXAudioSource.PlayOneShot (dealingCard);

	}

	public IEnumerator PlayRhomaMulai(){

		rhomaAudioSource.PlayOneShot(rhomaMulai[Random.Range(0,rhomaMulai.Length)]);
		while (rhomaAudioSource.isPlaying)
			yield return null;
		
	}

	public IEnumerator PlayRhomaKecewa(){

		rhomaAudioSource.PlayOneShot(rhomaKecewa[Random.Range(0,rhomaKecewa.Length)]);
		while (rhomaAudioSource.isPlaying)
			yield return null;

	}

	public IEnumerator PlayRhomaPasang(){

		rhomaAudioSource.PlayOneShot(rhomaPasang[Random.Range(0,rhomaPasang.Length)]);
		while (rhomaAudioSource.isPlaying)
			yield return null;

	}

	public IEnumerator PlayRhomaSelesai(){

		rhomaAudioSource.PlayOneShot(rhomaSelesai[Random.Range(0,rhomaSelesai.Length)]);
		while (rhomaAudioSource.isPlaying)
			yield return null;

	}
}
