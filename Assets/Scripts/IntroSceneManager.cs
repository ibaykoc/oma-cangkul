﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class IntroSceneManager : MonoBehaviour {

	Image blackImage;
	[SerializeField] float fadeSpeed;
	void Awake () {
		blackImage = GetComponent<Image> ();
		blackImage.color = Color.black;
	}
	
	void Start () {
		StartCoroutine (IStart ());
	}

	IEnumerator IStart(){
		yield return new WaitForSeconds (1f);
		float t = 0;
		while (t < 1f) {
			blackImage.color = Color.Lerp (Color.black, Color.clear, t);
			t += fadeSpeed * Time.deltaTime;
			yield return null;
		}
		t = 1f;
		blackImage.color = Color.clear;
		yield return new WaitForSeconds (3f);
		while (t > 0f) {
			blackImage.color = Color.Lerp (Color.black, Color.clear, t);
			t -= fadeSpeed * Time.deltaTime;
			yield return null;
		}
		blackImage.color = Color.black;
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}
}
