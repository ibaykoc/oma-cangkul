﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MySceneManager : MonoBehaviour {

	public static MySceneManager instance;

	void Awake(){

		if (instance == null)
			instance = this;
		else
			Destroy (gameObject);

		DontDestroyOnLoad (this);

	}

	public void LoadScene(string _sceneName){

		SceneManager.LoadScene (_sceneName);

	}

	public void LoadScene(int _sceneId){

		SceneManager.LoadScene (_sceneId);

	}

	public void Quit(){

		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}
}
