﻿using UnityEngine;
using System;
using System.Collections;

public class Card : MonoBehaviour {

	public enum CardState
	{
		inHand,
		inGame,
		inBin
	}

	SpriteRenderer sprRend;

	public Sprite sprite;
	public int typeId;		//Diamond : 0, Club : 1, Heart : 2, Spade : 3
	public int score;
	//public int inHolderId;
	public CardState state = CardState.inHand;
	public bool transitioning = false;

	public int handId;

	bool show = true;
	public bool Show {
		get{

			return show;

		}
		set{

			show = value;
			Open (value);

		}
	}

	Vector3 desirePos;
	Vector3 startPos;
	float t = 0f;

	void OnEnable(){

		if (sprRend == null)
			sprRend = GetComponent<SpriteRenderer> ();

	}

	public void UpdateOrder(){

		desirePos = transform.position;
		desirePos.z = (transform.position.y + transform.position.x) / 100f;
		transform.position = desirePos;
		//sprRend.sortingOrder = Mathf.RoundToInt (transform.position.y * 100f) * -1; 

	}

	void Open(bool _open){

		if (_open)
			sprRend.sprite = sprite;
		else
			sprRend.sprite = DeckPrefab.instance.cardBackSpr;

	}

	public void Move(Vector3 _pos){

			StartCoroutine (IMove (_pos));

	}

	public IEnumerator IMove(Vector3 _pos){

		t = 0f;
		startPos = transform.position;
		_pos.z = (_pos.x + _pos.y) / 100f;
		desirePos = _pos;
		if (transitioning)
			yield break;
		
		transitioning = true;

		while (t < 1) {

			transform.position = Vector3.Lerp (startPos, desirePos, t);
			t += .05f;
			yield return null;

		}
		transform.position = desirePos;
		transitioning = false;

	}

	public IEnumerator IMove(Vector3 _pos, GameManager.myDelegate action){

		t = 0f;
		startPos = transform.position;
		_pos.z = (_pos.x + _pos.y) / 100f;
		desirePos = _pos;
		if (transitioning)
			yield break;

		transitioning = true;

		while (t < 1) {

			transform.position = Vector3.Lerp (startPos, desirePos, t);
			t += .05f;
			yield return null;

		}
		transform.position = desirePos;
		transitioning = false;
		action ();
	}
}
