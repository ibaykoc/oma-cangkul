﻿using UnityEngine;
using System.Collections.Generic;

public class Bin : MonoBehaviour {

	public Vector3 minPos, maxPos;
	public List<Card> cards = new List<Card>();

	public void Add(Card _card){

		_card.state = Card.CardState.inBin;
		cards.Add (_card);
		for (int i = 0; i < cards.Count; i++) {
			cards[i].Move(FindPosition(i+1,cards.Count+1));
		}

	}

	Vector3 FindPosition(int _id, int _total){

		return Vector3.Lerp(maxPos,minPos,(float)_id/(float)_total);

	}
}
