﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuUI : MonoBehaviour {

	//Stats
	[SerializeField] Text winLossText;
	[SerializeField] Text totalPlayTimeText;
	[SerializeField] Text LongestMatchText;
	[SerializeField] Text shortestMatchText;

	public void Play(){

		MySceneManager.instance.LoadScene ("Main Game");

	}

	public void Quit(){

		MySceneManager.instance.Quit();

	}

	public void UpdateStats(){

//		print ("Stats Updated");
		float totalPlayTime = PlayerPrefs.GetFloat ("Total Play Time");
		float longestTime = PlayerPrefs.GetFloat ("Longest Match Time");
		float shortestTime = PlayerPrefs.GetFloat ("Shortest Match Time");
		winLossText.text = "Win / Loss : " + PlayerPrefs.GetInt("Win").ToString() + "/" + PlayerPrefs.GetInt("Loss").ToString();
		totalPlayTimeText.text = "Total Play Time : " + string.Format ("{0}:{1:00}", (int)totalPlayTime / 60, (totalPlayTime > 60f)?(int)totalPlayTime % 60 : (int)totalPlayTime);
		LongestMatchText.text = "Longest Match Time : " + string.Format ("{0}:{1:00}", (int)longestTime / 60, (longestTime > 60f)?(int)longestTime % 60 : (int)longestTime);
		shortestMatchText.text = "Shortest Match Time : " + string.Format ("{0}:{1:00}", (int)shortestTime / 60, (shortestTime > 60f) ? (int)shortestTime % 60 : (int)shortestTime);
	}
}
