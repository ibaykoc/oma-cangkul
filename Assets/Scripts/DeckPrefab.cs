﻿using UnityEngine;
using System.Collections;

public class DeckPrefab : MonoBehaviour {

	public static DeckPrefab instance;

	public Sprite cardBackSpr;
	public GameObject cardBack;
	public GameObject hightlightPrefab;

	public Card joker;
	public Card[] diamonds;
	public Card[] clubs;
	public Card[] hearts;
	public Card[] spades;

	Transform highlight;

	void Awake(){

		if (instance == null)
			instance = this;
		else
			Destroy (gameObject);

	}

//	public void SpawnHighLight(Vector3 _pos){
//
//		if (highlight == null){
//			GameObject go = Instantiate (hightlightPrefab, _pos, Quaternion.identity) as GameObject;
//			highlight = go.transform;
//		}
//		else{
//			highlight.gameObject.SetActive (true);
//			highlight.position = _pos;
//		}
//	}
//
//	public void HideHightlight(){
//
//		if (highlight == null)
//			return;
//		highlight.gameObject.SetActive (false);
//
//	}
}
