﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class GameUI : MonoBehaviour {

	public static GameUI instance;

	[SerializeField] GameObject instructionPanel;
	[SerializeField] Text instructionText;

	[SerializeField] GameObject pausePanel;

	[SerializeField] Text musicText;
	[SerializeField] Slider musicSlider;
	[SerializeField] Text SFXText;
	[SerializeField] Slider SFXSlider;

	WaitForSeconds oneSecWait = new WaitForSeconds (1f);

	void Awake(){

		if (instance == null)
			instance = this;
		else
			Destroy (gameObject);

	}

	void OnEnable(){

		GameManager.instance.OnDoneTurn += ShowTurnInfo;
		GameManager.instance.OnGameEnd += ShowWhoWin;
	}

	void OnDisable(){

		GameManager.instance.OnDoneTurn -= ShowTurnInfo;
		GameManager.instance.OnGameEnd -= ShowWhoWin;
	}

	public void ShowTurnInfo(){

		StartCoroutine (IShowTurnInfo());

	}

	IEnumerator IShowTurnInfo(){
		//print ("Mulai Tunjukan giliran");
		instructionText.text = (GameManager.instance.isPlayerTurn) ? "Your Turn" : "CPU's Turn";
		instructionPanel.SetActive (true);
		yield return oneSecWait;
		instructionPanel.SetActive (false);
		//print ("Selesai Tunjukan giliran");
		GameManager.instance.TurnStart();
	}

	public void ShowWhoWin(){

		StartCoroutine (IShowWhoWin ());
	}

	IEnumerator IShowWhoWin(){
		//print ("Mulai Tunjukan giliran");
		instructionText.text = (GameManager.instance.isPlayerTurn) ? "CPU WIN" : "YOU WIN";
		instructionPanel.SetActive (true);
		yield return StartCoroutine(AudioManager.instance.PlayRhomaSelesai ());
		yield return oneSecWait;

		instructionPanel.SetActive (false);
		//print ("Selesai Tunjukan giliran");
		MySceneManager.instance.LoadScene("Main Menu");
	}

	public void ShowPausePanel(bool _show){

		pausePanel.SetActive (_show);

	}

	#region Button Callback
	public void Resume(){

		GameManager.instance.Pause ();

	}

	public void Restart(){

		GameManager.instance.Restart ();

	}

	public void MainMenu(){

		MySceneManager.instance.LoadScene ("Main Menu");
	}

	public void ToggleMusic(){
		
		if (AudioManager.instance.BGAudioSource.isPlaying) {

			AudioManager.instance.BGAudioSource.Stop ();

		} else {

			AudioManager.instance.BGAudioSource.Play ();

		}
		musicText.text = (AudioManager.instance.BGAudioSource.isPlaying) ? "Music : On" : "Music : Off";

	}

	public void UpdateMusicVolume(){

		AudioManager.instance.BGAudioSource.volume = musicSlider.value/3f;

	}

	public void ToggleSFX(){

		AudioManager.instance.sfxOn = !AudioManager.instance.sfxOn;
		SFXText.text = (AudioManager.instance.sfxOn) ? "SFX : On" : "SFX : Off";

	}

	public void UpdateSFXVolume(){

		AudioManager.instance.SFXAudioSource.volume = SFXSlider.value;

	}
	#endregion


}
