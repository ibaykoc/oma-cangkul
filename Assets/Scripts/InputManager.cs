﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

	GameObject selectedGO;
	Card selectedCard;
	Vector3 offset;
	Vector3 startPos;

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape) && selectedCard == null)
			GameManager.instance.Pause ();
		if (!GameManager.instance.isPlayerTurn || GameManager.instance.pause)
			return;
		if(Input.GetMouseButtonDown(0)){
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (hit.transform != null) {
				if (hit.transform.CompareTag ("Card")) {

					selectedGO = hit.collider.gameObject;
					selectedCard = selectedGO.GetComponent<Card> ();
					offset = selectedGO.transform.position - Camera.main.ScreenToWorldPoint (Input.mousePosition);
					startPos = selectedGO.transform.position;
				}
				if (hit.transform.CompareTag ("Deck") && GameManager.instance.inGameCard.cards.Count > 0 && !GameManager.instance.isHaveValidCard()) {
					AudioManager.instance.PlayDealingCard ();
					GameManager.instance.hands [0].Add (GameManager.instance.AmbilKartu (0), true);
				}
			}
		}

		if (selectedGO != null && selectedCard.Show && !selectedCard.transitioning) {
			selectedGO.transform.position = Camera.main.ScreenToWorldPoint (Input.mousePosition) + offset;
			selectedCard.UpdateOrder ();
		}

		if (Input.GetMouseButtonUp (0)) {

			if (selectedGO != null) {
				if (positionIsInside (selectedGO.transform.position, GameManager.instance.inGameCard.minPos, GameManager.instance.inGameCard.maxPos) && selectedCard.state != Card.CardState.inGame) {
					if (!GameManager.instance.PasangKartu (selectedCard)) {
						selectedCard.Move (startPos);
					}
				} else {
					selectedCard.Move (startPos);
				}
				selectedGO = null;
				selectedCard = null;
			}
		}
	}

	bool positionIsInside(Vector3 _pos,Vector2 _min,Vector2 _max){

		if (_pos.x > _min.x && _pos.x < _max.x && _pos.y > _min.y && _pos.y < _max.y)
			return true;
		return false;

	}
}
