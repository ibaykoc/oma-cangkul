﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hand : MonoBehaviour {

	public Vector3 minPos, maxPos;
	public List<Card> cards = new List<Card>();
	public bool hasPutCard = false;

	public bool addingCard;

	public void Add(Card _card, bool _show){
		_card.Show = _show;
		cards.Add (_card);
		UpdatePosition ();

	}

	public IEnumerator IAdd(Card _card, bool _show){
		addingCard = true;
		//_card.inHolderId = cards.Count;
		_card.Show = _show;
		cards.Add (_card);
		yield return StartCoroutine (IUpdatePosition());
		//UpdatePosition ();
		addingCard = false;
	}

	public IEnumerator IAdd(Card[] _cards, bool _show){

		addingCard = true;

		for (int i = 0; i < _cards.Length; i++) {
			_cards[i].Show = _show;
			cards.Add (_cards[i]);
		}

		yield return StartCoroutine (IUpdatePosition());
		//UpdatePosition ();
		addingCard = false;
	}

	Vector3 FindPosition(int _id, int _total){

		return Vector3.Lerp(minPos,maxPos,(float)_id/(float)_total);

	}

	public void Remove(Card _card){

		cards.Remove (_card);
		UpdatePosition ();
	}

	public IEnumerator IUpdatePosition(){

		for (int i = cards.Count - 1; i >= 0; i--) {
			AudioManager.instance.PlayDealingCard ();
			yield return StartCoroutine (cards[i].IMove(FindPosition(i + 1,cards.Count + 1)));
		}

	}

	public void UpdatePosition(){

		for (int i = 0; i < cards.Count; i++) {
			cards[i].Move(FindPosition(i+1,cards.Count+1));
		}

	}
}
